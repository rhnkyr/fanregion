<?php

namespace App\Http\Controllers\Api\V1;

use App\Mail\RegisterConfirmation;
use Illuminate\Http\Request;
use App\Models\User;
use App\Jobs\SendRegisterEmail;
use Illuminate\Support\Facades\Mail;

class AuthController extends BaseController
{

    public function initLogin(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }

        // check user is active or not
        $user = User::where([
            ['email', '=', $request->email],
            ['active', '=', 1]
        ])->first();

        if (!$user) {
            return $this->response->errorUnauthorized();
        }

        if ($user->password_selected === 0) {
            $user->password_selected = 1;
            $user->save();
        }

        $credentials = $request->only('email', 'password');

        if (!$token = \Auth::attempt($credentials)) {
            $this->response->errorUnauthorized(trans('auth.incorrect'));
        }


        $result['data'] = [
            'user'  => $user,
            'token' => $token,
        ];

        return $this->response->array($result)->setStatusCode(201);
    }

    /**
     * @api {post} /authorizations register new user
     * @apiDescription register new user
     * @apiGroup Auth
     * @apiPermission none
     * @apiParam {String} name
     * @apiParam {Email} email
     * @apiParam {String} password
     * @apiVersion 0.2.0
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 201 Created
     *     {
     *         "data": {
     *             "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbHVtZW4tYXBpLWRlbW8uZGV1L2FwaS9hdXRob3JpemF0aW9ucyIsImlhdCI6MTQ4Mzk3NTY5MywiZXhwIjoxNDg5MTU5NjkzLCJuYmYiOjE0ODM5NzU2OTMsImp0aSI6ImViNzAwZDM1MGIxNzM5Y2E5ZjhhNDk4NGMzODcxMWZjIiwic3ViIjo1M30.hdny6T031vVmyWlmnd2aUr4IVM9rm2Wchxg5RX_SDpM",
     *             "expired_at": "2017-03-10 15:28:13",
     *             "refresh_expired_at": "2017-01-23 15:28:13"
     *         }
     *     }
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 401
     *     {
     *       "error": "Register failed"
     *     }
     * @param Request $request
     * @return void
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function register(Request $request)
    {
        // \Log::info(json_encode($request->all()));
        // \Log::info($request);

        $validator = \Validator::make($request->input(), [
            'name'              => 'required|string',
            'email'             => 'required|email|unique:users',
            'social_media_link' => 'required|string'
            //'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }

        //$password = $request->input('password');

        $attributes = [
            'name'                  => $request->input('name'),
            'email'                 => $request->input('email'),
            'social_media_link'     => $request->input('social_media_link'),
            'total_followers'       => $request->input('total_followers'),
            'validation_identifier' => sha1($request->input('email') . $request->input('name')),
            //'password' => app('hash')->make($password),
            'active'                => 0,
            'password_selected'     => 0
        ];

        $user = User::create($attributes);
        //$token = JWTAuth::fromUser($user);

        //todo : malining açılacak
        // Send the email after the user has successfully registered
        //dispatch(new SendRegisterEmail($user));
        //Mail::to('test+receiver@email.es')->send(new RegisterConfirmation($user));


        $result['data'] = [
            'status' => true
        ];
        return $this->response->array($result)->setStatusCode(201);
    }
}