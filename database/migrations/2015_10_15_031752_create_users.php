<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->index();
            $table->string('email')->unique()->index();
            $table->string('password');
            $table->string('social_media_link');
            $table->integer('total_followers');
            $table->string('validation_identifier');
            $table->tinyInteger('active')->default(0);
            $table->tinyInteger('password_selected')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('users');
    }
}
